import React from 'react';

const UserInput = (props) => {
    const inputStyle = {
        border: "2px solid grey",
        padding: "10px",
        marginBottom: "5px"
    };

    return (
            <input 
                type="text"
                onChange={props.change}
                value={props.currentName}
                style={inputStyle}
            />
    )
}

export default UserInput
