import React from 'react';
import './UserOutput.css';

const UserOutput = (props) => {
    return (
        <div className="UserOutput">
            <p><strong>This is {props.username}</strong></p>
            <p>It is a dog!</p>
        </div>
    )
}

export default UserOutput
